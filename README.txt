CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

CKEditor Equation Editor is an extension to the Drupal 8 CKEditormodule.
This module integrates the Equation Editor library into your Drupal site.
Equation Editor is the modern JavaScript based LaTex rendering solution
for the internet.
This Equation Editor provides a toolbar with common mathematical operators
and symbols, to help write stunning equations using the LaTeX markup.
A high resolution preview of your equation is shown within the editor,
which is inserted into your document when you click 'Ok'.
This editor is powered by the CodeCogs Equation Engine, the world's leading
equation rendering web-service.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/eqneditor

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/eqneditor


REQUIREMENTS
------------

This module doesn't require any module outside of Drupal core.


INSTALLATION
------------

   1. Copy the ckeditor_eqneditor folder to your modules directory.
   2. Download eqneditor library from http://ckeditor.com/addon/eqneditor.
   3. Copy the downloaded content that is eqneditr folder in the root
      libraries folder (/libraries).
   4. Go to admin/modules and enable the module.


CONFIGURATION
-------------

   1. Go to admin/config/content/formats and configure the desired profile.
   2. Move a button into the Active toolbar.
   3. Clear your browser's cache, and a new button( fx ) will appear in your
      toolbar.


MAINTAINERS
-----------

 * Gaurav Kapoor (gaurav.kapoor) - https://www.drupal.org/u/gauravkapoor

Supporting organizations:

 * OpenSense Labs - https://www.drupal.org/opensense-labs
